# README #

The server companion to the [Birthday Calendar Client](https://bitbucket.org/Snurpels/birthday-calendar).

* Set up a MySQL or MariaDB server
* Create a user `bdcal` and a database `bdcal`

Now either:
### From Backup ###
* Unzip the backed up version
* Open `wildfly/standalone/configuration/standalone.xml`, find the subsection titled `<subsystem xmlns="urn:jboss:domain:datasources:4.0">` and replace the database users username and password (under `<security>`) with the ones you created at the beginning.
### From Scratch ###

* Download and extract [Wildfly 10.1](http://download.jboss.org/wildfly/10.1.0.Final/wildfly-10.1.0.Final.zip)
* Find the following snippet in the `wildfly/standalone/configuration/standalone.xml` file:

```
#!xml

<default-bindings context-service="java:jboss/ee/concurrency/context/default" datasource="java:jboss/datasources/ExampleDS" managed-executor-service="java:jboss/ee/concurrency/executor/default" managed-scheduled-executor-service="java:jboss/ee/concurrency/scheduler/default" managed-thread-factory="java:jboss/ee/concurrency/factory/default"/>
```
* Replace it with the following:

```
#!xml

<default-bindings context-service="java:jboss/ee/concurrency/context/default" managed-executor-service="java:jboss/ee/concurrency/executor/default" managed-scheduled-executor-service="java:jboss/ee/concurrency/scheduler/default" managed-thread-factory="java:jboss/ee/concurrency/factory/default"/>
```
* Find the subsection titled `<subsystem xmlns="urn:jboss:domain:datasources:4.0">` and replace it with the following. Remember to adjust the <connection-url> section with your actual db location and port (default port for MySQL is 3306). Specify the database users username and password you created at the beginning under the <security> section.

```
#!xml
<subsystem xmlns="urn:jboss:domain:datasources:4.0">
	<datasources>
		<datasource jndi-name="java:jboss/datasources/BDCal" pool-name="BDCal" enabled="true" use-java-context="true">
			<connection-url>jdbc:mysql://localhost:3306/bdcal?useUnicode=yes&amp;useLegacyDatetimeCode=false&amp;serverTimezone=Europe/Berlin&amp;characterEncoding=UTF-8</connection-url>
			<driver>com.mysql</driver>
			<transaction-isolation>TRANSACTION_READ_COMMITTED</transaction-isolation>
			<pool>
				<min-pool-size>10</min-pool-size>
				<max-pool-size>100</max-pool-size>
				<prefill>true</prefill>
			</pool>
			<security>
				<user-name>bdcal</user-name>
				<password>bdcalpw</password>
			</security>
			<statement>
				<prepared-statement-cache-size>32</prepared-statement-cache-size>
				<share-prepared-statements>true</share-prepared-statements>
			</statement>
		</datasource>
		<drivers>
			<driver name="com.mysql" module="mysql.mysql-connector-java">
				<xa-datasource-class>com.mysql.jdbc.jdbc2.optional.MysqlXADataSource</xa-datasource-class>
			</driver>
		</drivers>
	</datasources>
</subsystem>

```
* Download the latest [MySQL Connector Jar](https://dev.mysql.com/downloads/connector/j/) and place it so that it (*just* the jar) resides in `WILDFLYROOT\modules\system\layers\base\mysql\mysql-connector-java\main` (you will probably have to create the last few folders)
* Create an XML file in the same folder named `module.xml` with the following content (adjusted for the actual file name version of your jar):

```
#!xml

<?xml version="1.0" encoding="UTF-8"?>
<module xmlns="urn:jboss:module:1.1" 
        name="mysql.mysql-connector-java">
    <resources>
        <resource-root path="mysql-connector-java-6.0.4.jar"/>
    </resources>
    <dependencies>
        <module name="javax.api"/>
        <module name="javax.transaction.api"/>
        <module name="javax.servlet.api" optional="true"/>
    </dependencies>
</module>
```
* Find the subsection titled `<subsystem xmlns="urn:jboss:domain:naming:2.0">` and replace it with the following. Remember to adjust the password - this is the password that users of the client have to enter in order to see moeny values:

```
#!xml

<subsystem xmlns="urn:jboss:domain:naming:2.0">
	<bindings>
		<simple name="java:global/BDCal/password" value="PASSWORDGOESHERE"/>
	</bindings>
	<remote-naming/>
</subsystem>

```