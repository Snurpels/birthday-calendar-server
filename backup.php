<?php

$startTime = microtime(TRUE);
$databases = array();

// Add stuff to back up here. Remember that the user specified in $databaseLogin needs to have SELECT and LOCK permissions for the database you want it to back up.

$databases[] = "bdcal";// ----------------------------------------------------------
// Only modify the below if needed

$databaseLogin = "/home/pi/backup.my.cnf";
$compression = "7z a -mx=0 -mmt=on -m0=BCJ -m1=LZMA:d=21 -ms -t7z -si -pkrankeBanane";
$backupMain = "/mnt/austauschordner";
$backupDate = date('Y-m-d');
$backupDir = "{$backupMain}/{$backupDate}";

// Do the backup

foreach ($databases as $database) {
	echo "Backing up {$database}...";

	$outFile = "{$backupDate} {$database}.7z";

	if (!is_dir($backupDir))
		mkdir($backupDir, 0700, TRUE);
	exec("mysqldump --defaults-file={$databaseLogin} --lock-all-tables --opt {$database} | {$compression} '{$backupDir}/{$outFile}'");
	echo "done!\n";
}

echo number_format((microtime(TRUE) - $startTime)) . " seconds.\n";?>
