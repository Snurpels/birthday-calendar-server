package de.rpk_aachen.birthday.server.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import de.rpk_aachen.birthday.model.Person;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = { "id" })
@ToString
public class SqlPerson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private long      id;
    @Column
    private String    name;
    @Column
    private LocalDate birthday;
    @Column
    private boolean   alumn;
    @Column
    private int       euros;

    public Person asPerson() {
        return new Person(name, birthday, alumn, euros);
    }

    public SqlPerson(Person p) {
        birthday = p.getBirthday();
        euros = p.getEuros();
        name = p.getName();
        alumn = p.isAlumn();
    }
}