package de.rpk_aachen.birthday.server.model;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import de.rpk_aachen.birthday.model.tx.Deposit;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class SqlDeposit {
    public SqlDeposit(Deposit d, SqlPerson sp) {
        date = d.getDate();
        person = sp;
        amount = d.getAmount();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long          id;

    @Column
    private LocalDateTime date;

    @Column
    private int           amount;

    @ManyToOne(optional = false, cascade = CascadeType.ALL, targetEntity = SqlPerson.class)
    @JoinColumn(name = "depositor", referencedColumnName = "id")
    private SqlPerson     person;

    public Deposit asDeposit() {
        Deposit d = new Deposit(person.asPerson(), amount);
        d.setDate(date);
        return d;
    }
}