package de.rpk_aachen.birthday.server.model;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import de.rpk_aachen.birthday.model.tx.Deposit;
import de.rpk_aachen.birthday.model.tx.Withdrawal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class SqlWithdrawal {
	public SqlWithdrawal(Withdrawal w) {
		date = w.getDate();
		amount = w.getAmount();
		reason = w.getReason();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column
	private LocalDateTime date;

	@Column
	private int amount;

	@Column
	private String reason;

	public Withdrawal asWithdrawal() {
		Withdrawal w = new Withdrawal(amount, reason);
		w.setDate(date);
		return w;
	}
}