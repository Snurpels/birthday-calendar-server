package de.rpk_aachen.birthday.server.model;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import de.rpk_aachen.birthday.model.tx.AddPerson;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class SqlAddPerson {
    public SqlAddPerson(AddPerson p, SqlPerson sp) {
        date = p.getDate();
        person = sp;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long          id;

    @Column
    private LocalDateTime date;

    @OneToOne(optional = false, cascade = CascadeType.ALL, targetEntity = SqlPerson.class)
    private SqlPerson     person;

    public AddPerson asAddPerson() {
        AddPerson ap = new AddPerson(person.asPerson());
        ap.setDate(date);
        return ap;
    }
}