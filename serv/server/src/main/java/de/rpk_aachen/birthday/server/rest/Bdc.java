package de.rpk_aachen.birthday.server.rest;

import static de.rpk_aachen.birthday.model.rest.Endpoints.GET_AUTHORIZED;
import static de.rpk_aachen.birthday.model.rest.Endpoints.GET_CHANGEPEOPLE;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import de.rpk_aachen.birthday.model.tx.ChangePerson;
import de.rpk_aachen.birthday.server.model.SqlChangePerson;
import de.rpk_aachen.birthday.server.util.Resources;

@Path("/")
@Stateless
@Produces({ "application/json" })
public class Bdc {
    protected static boolean authorized(String password) {
        return Resources.password.equals(password);
    }

    @Inject
    EntityManager em;

    /**
     * Checks whether the password supplied will allow admin access
     *
     * @param password
     *            the password matching the one specified in
     *            java:global/BDCal/password
     * @return true if the password matches
     */
    @GET
    @Path(GET_AUTHORIZED)
    public boolean getAuthorized(@QueryParam("p") String password) {
        return authorized(password);
    }

    /**
     * Returns a list of all changes of people. If the password is wrong or not
     * supplied, all people will show up with no money.
     *
     * @param password
     *            the password matching the one specified in
     *            java:global/BDCal/password
     * @return the list of people
     */
    @GET
    @Path(GET_CHANGEPEOPLE)
    public List<ChangePerson> getChangePeople(@QueryParam("p") String password) {
        List<SqlChangePerson> txChangePeople = em
                .createQuery("select cp from SqlChangePerson cp", SqlChangePerson.class).getResultList();
        if (authorized(password))
            return txChangePeople.stream().map(cp -> cp.asChangePerson()).collect(Collectors.toList());
        else
            return txChangePeople.stream().map(cp -> cp.asChangePerson()).peek(cp -> {
                cp.getPerson().setEuros(0);
                cp.getPersonNew().setEuros(0);
            }).collect(Collectors.toList());
    }
}
