package de.rpk_aachen.birthday.server.rest;

import static de.rpk_aachen.birthday.model.rest.Endpoints.TX_DEPOSIT;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import de.rpk_aachen.birthday.model.tx.Deposit;
import de.rpk_aachen.birthday.server.model.SqlDeposit;
import de.rpk_aachen.birthday.server.model.SqlPerson;

@Path(TX_DEPOSIT)
@Stateless
@Produces({ "application/json" })
public class TxDeposit {
    @Inject
    EntityManager em;

    /**
     * Adds a {@link de.rpk_aachen.birthday.model.tx.Deposit Deposit}
     * transaction with the specified data. If the password is wrong or not
     * supplied, the transaction will not be added.
     *
     * @param deposit
     *            transaction to be added
     * @param password
     *            the password matching the one specified in
     *            java:global/BDCal/password
     * @return true if the transaction was added, false if there was a password
     *         mismatch
     */
    @POST
    public boolean addTxDeposit(@QueryParam("p") String password, Deposit d) {
        if (Bdc.authorized(password)) {
            TypedQuery<SqlPerson> query = em.createQuery("select p from SqlPerson p where p.name like :name",
                    SqlPerson.class);
            query.setParameter("name", d.getPerson().getName());
            SqlPerson sp = query.getSingleResult();
            sp.setEuros(sp.getEuros() + d.getAmount());
            SqlDeposit sd = new SqlDeposit(d, sp);
            em.merge(sd);
            return true;
        } else
            return false;
    }

    /**
     * Returns a list of all deposits. If the password is wrong or not supplied,
     * all deposits will show up with a deposit value of zero.
     *
     * @param password
     *            the password matching the one specified in
     *            java:global/BDCal/password
     * @return the list of deposits
     */
    @GET
    public List<Deposit> getDeposit(@QueryParam("p") String password) {
        List<SqlDeposit> txDeposit = em.createQuery("select d from SqlDeposit d", SqlDeposit.class).getResultList();
        if (Bdc.authorized(password))
            return txDeposit.stream().map(d -> d.asDeposit()).collect(Collectors.toList());
        else
            return txDeposit.stream().map(d -> d.asDeposit()).peek(d -> d.setAmount(0)).collect(Collectors.toList());
    }
}
