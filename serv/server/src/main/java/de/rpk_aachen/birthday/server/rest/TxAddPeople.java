package de.rpk_aachen.birthday.server.rest;

import static de.rpk_aachen.birthday.model.rest.Endpoints.TX_ADDPERSON;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;

import de.rpk_aachen.birthday.model.tx.AddPerson;
import de.rpk_aachen.birthday.server.model.SqlAddPerson;
import de.rpk_aachen.birthday.server.model.SqlPerson;

@Path(TX_ADDPERSON)
@Stateless
@Produces({ "application/json" })
public class TxAddPeople {
    @Inject
    EntityManager em;

    /**
     * Adds an {@link de.rpk_aachen.birthday.model.tx.AddPerson AddPerson}
     * transaction with the specified data. If the password is wrong or not
     * supplied, the transaction will not be added.
     *
     * @param addPerson
     *            transaction to be added
     * @param password
     *            the password matching the one specified in
     *            java:global/BDCal/password
     * @return true if the transaction was added, false if there was a password
     *         mismatch
     */
    @POST
    public boolean addTxAddPerson(@Context HttpServletRequest ctx, @QueryParam("p") String password, AddPerson ap) {
        if (Bdc.authorized(password)) {
            TypedQuery<SqlPerson> query = em.createQuery("select p from SqlPerson p where p.name like :name",
                    SqlPerson.class);
            query.setParameter("name", ap.getPerson().getName());
            SqlPerson sp = query.getSingleResult();
            SqlAddPerson sap = new SqlAddPerson(ap, sp);
            em.merge(sap);
            return true;
        } else
            return false;
    }

    /**
     * Returns a list of all additions of people. If the password is wrong or
     * not supplied, all people will show up with no money.
     *
     * @param password
     *            the password matching the one specified in
     *            java:global/BDCal/password
     * @return the list of people
     */
    @GET
    public List<AddPerson> getAddPeople(@QueryParam("p") String password) {
        List<SqlAddPerson> txAddPeople = em.createQuery("select ap from SqlAddPerson ap", SqlAddPerson.class)
                .getResultList();
        if (Bdc.authorized(password))
            return txAddPeople.stream().map(ap -> ap.asAddPerson()).collect(Collectors.toList());
        else
            return txAddPeople.stream().map(ap -> ap.asAddPerson()).peek(ap -> ap.getPerson().setEuros(0))
                    .collect(Collectors.toList());
    }
}
