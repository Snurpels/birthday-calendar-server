package de.rpk_aachen.birthday.server.rest;

import static de.rpk_aachen.birthday.model.rest.Endpoints.TX_PAYOUT;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import de.rpk_aachen.birthday.model.tx.Payout;
import de.rpk_aachen.birthday.server.model.SqlPayout;
import de.rpk_aachen.birthday.server.model.SqlPerson;

@Path(TX_PAYOUT)
@Stateless
@Produces({ "application/json" })
public class TxPayout {
    @Inject
    EntityManager em;

    /**
     * Adds a {@link de.rpk_aachen.birthday.model.tx.Payout Payout} transaction
     * with the specified data. If the password is wrong or not supplied, the
     * transaction will not be added.
     *
     * @param payout
     *            transaction to be added
     * @param password
     *            the password matching the one specified in
     *            java:global/BDCal/password
     * @return true if the transaction was added, false if there was a password
     *         mismatch
     */
    @POST
    public boolean addTxPayout(@QueryParam("p") String password, Payout p) {
        if (Bdc.authorized(password)) {
            // First replace incoming list of people with managed entities
            TypedQuery<SqlPerson> query = em.createQuery("select p from SqlPerson p WHERE p.name IN (:nameList)",
                    SqlPerson.class);
            query.setParameter("nameList", p.getPayers().stream().map(py -> py.getName()).collect(Collectors.toList()));
            List<SqlPerson> payers = query.getResultList();
            payers.stream().forEach(payer -> payer.setEuros(payer.getEuros() - 1));
            // Next replace recipient with the managed entity
            query = em.createQuery("select p from SqlPerson p where p.name like :name", SqlPerson.class);
            query.setParameter("name", p.getPerson().getName());
            SqlPerson sp = query.getSingleResult();
            SqlPayout sqlP = new SqlPayout(p, sp, payers);
            em.merge(sqlP);
            return true;
        } else
            return false;
    }

    /**
     * Returns a list of all birthday payouts. If the password is wrong or not
     * supplied, all payouts will show up with a value of zero and an empty list
     * of contributers.
     *
     * @param password
     *            the password matching the one specified in
     *            java:global/BDCal/password
     * @return the list of people
     */
    @GET
    public List<Payout> getPayout(@QueryParam("p") String password) {
        List<SqlPayout> txPayout = em.createQuery("select p from SqlPayout p", SqlPayout.class).getResultList();
        if (Bdc.authorized(password))
            return txPayout.stream().map(p -> p.asPayout()).collect(Collectors.toList());
        else
            return txPayout.stream().map(p -> p.asPayout()).peek(p -> {
                p.setAmount(0);
                p.getPayers().clear();
            }).collect(Collectors.toList());
    }
}
