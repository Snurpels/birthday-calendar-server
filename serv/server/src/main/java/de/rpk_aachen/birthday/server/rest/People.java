package de.rpk_aachen.birthday.server.rest;

import static de.rpk_aachen.birthday.model.rest.Endpoints.PEOPLE;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;

import de.rpk_aachen.birthday.model.ModifiedPerson;
import de.rpk_aachen.birthday.model.Person;
import de.rpk_aachen.birthday.model.tx.ChangePerson;
import de.rpk_aachen.birthday.server.model.SqlChangePerson;
import de.rpk_aachen.birthday.server.model.SqlPerson;

@Path(PEOPLE)
@Stateless
@Produces({ "application/json" })
public class People {
    @Inject
    EntityManager  em;

    @Inject
    private Logger log;

    /**
     * Adds a {@link de.rpk_aachen.birthday.model.Person Person} with the
     * specified data. If the password is wrong or not supplied, the person will
     * not be added.
     *
     * @param person
     *            person to be added
     * @param password
     *            the password matching the one specified in
     *            java:global/BDCal/password
     * @return true if the person was added, false if there was a password
     *         mismatch
     */
    @POST
    public boolean addPerson(@Context HttpServletRequest ctx, @QueryParam("p") String password, Person p) {
        if (Bdc.authorized(password)) {
            SqlPerson sqlP = new SqlPerson(p);
            em.merge(sqlP);
            log.info(String.format(
                    "%s hat den folgenden Benutzer hinzugefügt: %s mit %,d €, Geburtstag am %4$td/%4$tm/19XX",
                    ctx.getRemoteAddr(), p.getName(), p.getEuros(), p.getBirthday()));
            return true;
        } else {
            log.warning(String.format(
                    "%s hat versucht, mit einem falschen Password den folgenden Benutzer hinzuzufügen: %s mit %,d €",
                    ctx.getRemoteAddr(), p.getName(), p.getEuros()));
            return false;
        }
    }

    /**
     * Returns a list of all people. If the password is wrong or not supplied,
     * all people will show up as having no money.
     *
     * @param password
     *            the password matching the one specified in
     *            java:global/BDCal/password
     * @return the list of people
     */
    @GET
    public List<Person> getPeople(@QueryParam("p") String password) {
        List<SqlPerson> txPeople = em.createQuery("select p from SqlPerson p", SqlPerson.class).getResultList();
        if (Bdc.authorized(password))
            return txPeople.stream().map(p -> p.asPerson()).collect(Collectors.toList());
        else
            return txPeople.stream().map(p -> p.asPerson()).peek(p -> {
                if (p.getEuros() < 2) {
                    p.setEuros(0);
                } else {
                    p.setEuros(1);
                }
            }).filter(p -> p.getEuros() > 0).collect(Collectors.toList());
    }

    /**
     * Update a {@link de.rpk_aachen.birthday.model.Person Person} with new
     * data. If the password is wrong or not supplied, the transaction will not
     * be added.
     *
     * @param person
     *            the person to be updated
     * @param password
     *            the password matching the one specified in
     *            java:global/BDCal/password
     * @return true if the person was updated, false if there was a password
     *         mismatch
     */
    @PUT
    public boolean updatePerson(@QueryParam("p") String password, ModifiedPerson mp) {
        if (Bdc.authorized(password)) {
            TypedQuery<SqlPerson> query = em.createQuery("select p from SqlPerson p where p.name like :name",
                    SqlPerson.class);
            query.setParameter("name", mp.getOldPerson().getName());
            SqlPerson sp = query.getSingleResult();

            Person pNew = mp.getNewPerson();
            sp.setName(pNew.getName());
            sp.setBirthday(pNew.getBirthday());
            sp.setEuros(pNew.getEuros());
            sp.setAlumn(pNew.isAlumn());
            System.out.println(sp);
            em.merge(sp);

            ChangePerson cp = new ChangePerson(mp.getOldPerson(), pNew);
            SqlChangePerson scp = new SqlChangePerson(cp);
            em.merge(scp);

            return true;
        } else
            return false;
    }
}
