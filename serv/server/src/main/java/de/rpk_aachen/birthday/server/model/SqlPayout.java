package de.rpk_aachen.birthday.server.model;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import de.rpk_aachen.birthday.model.tx.Payout;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class SqlPayout {
    @Column
    private int             amount;

    @Column
    private LocalDateTime   date;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long            id;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "payers", referencedColumnName = "id")
    private List<SqlPerson> payers;

    @ManyToOne(optional = false, cascade = CascadeType.ALL, targetEntity = SqlPerson.class)
    private SqlPerson       person;

    public SqlPayout(Payout p, SqlPerson person, List<SqlPerson> payers) {
        date = p.getDate();
        this.person = person;
        this.payers = payers;
        amount = p.getAmount();
    }

    public Payout asPayout() {
        Payout p = new Payout(person.asPerson(), amount,
                payers.stream().map(sp -> sp.asPerson()).collect(Collectors.toList()));
        p.setDate(date);
        return p;
    }
}