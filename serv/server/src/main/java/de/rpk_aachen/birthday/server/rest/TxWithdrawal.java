package de.rpk_aachen.birthday.server.rest;

import static de.rpk_aachen.birthday.model.rest.Endpoints.TX_WITHDRAWAL;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import de.rpk_aachen.birthday.model.tx.Deposit;
import de.rpk_aachen.birthday.model.tx.Withdrawal;
import de.rpk_aachen.birthday.server.model.SqlDeposit;
import de.rpk_aachen.birthday.server.model.SqlWithdrawal;

@Path(TX_WITHDRAWAL)
@Stateless
@Produces({ "application/json" })
public class TxWithdrawal {
	@Inject
	EntityManager em;

	/**
	 * Adds a {@link de.rpk_aachen.birthday.model.tx.Deposit Deposit}
	 * transaction with the specified data. If the password is wrong or not
	 * supplied, the transaction will not be added.
	 *
	 * @param deposit
	 *            transaction to be added
	 * @param password
	 *            the password matching the one specified in
	 *            java:global/BDCal/password
	 * @return true if the transaction was added, false if there was a password
	 *         mismatch
	 */
	@POST
	public boolean addWithdrawal(@QueryParam("p") String password, Withdrawal w) {
		if (Bdc.authorized(password)) {
			SqlWithdrawal sw = new SqlWithdrawal(w);
			em.merge(sw);
			return true;
		} else
			return false;
	}

	/**
	 * Returns a list of all deposits. If the password is wrong or not supplied,
	 * all deposits will show up with a deposit value of zero.
	 *
	 * @param password
	 *            the password matching the one specified in
	 *            java:global/BDCal/password
	 * @return the list of deposits
	 */
	@GET
	public List<Withdrawal> getWithdrawals(@QueryParam("p") String password) {
		List<SqlWithdrawal> txWithdrawal = em.createQuery("select w from SqlWithdrawal w", SqlWithdrawal.class)
				.getResultList();
		if (Bdc.authorized(password))
			return txWithdrawal.stream().map(SqlWithdrawal::asWithdrawal).collect(Collectors.toList());
		else
			return txWithdrawal.stream().map(SqlWithdrawal::asWithdrawal).peek(w -> w.setAmount(0)).collect(Collectors.toList());
	}
}
