package de.rpk_aachen.birthday.server.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/rest")
public class BdcActivator extends Application {}
