package de.rpk_aachen.birthday.server.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import de.rpk_aachen.birthday.model.Person;
import de.rpk_aachen.birthday.model.tx.ChangePerson;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class SqlChangePerson {
    public SqlChangePerson(ChangePerson cp) {
        date = cp.getDate();

        Person o = cp.getPerson();
        Person n = cp.getPersonNew();

        oldName = o.getName();
        oldBirthday = o.getBirthday();
        oldAlumn = o.isAlumn();
        oldEuros = o.getEuros();

        newName = n.getName();
        newBirthday = n.getBirthday();
        newAlumn = n.isAlumn();
        newEuros = n.getEuros();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long          id;

    @Column
    private LocalDateTime date;

    @Column
    private String        oldName, newName;
    @Column
    private LocalDate     oldBirthday, newBirthday;
    @Column
    private boolean       oldAlumn, newAlumn;
    @Column
    private int           oldEuros, newEuros;

    public ChangePerson asChangePerson() {
        return new ChangePerson(new Person(oldName, oldBirthday, oldAlumn, oldEuros),
                new Person(newName, newBirthday, newAlumn, newEuros), date);
    }
}