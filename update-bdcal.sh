#!/bin/sh

# Make temp dir
cd $(mktemp -d)

# Delete temp dir
echo "rm -rf $(pwd)\n" > /home/pi/clean.sh

git clone https://nv-aa@bitbucket.org/Snurpels/birthday-calendar.git
git clone https://nv-aa@bitbucket.org/Snurpels/birthday-calendar-server.git

cd birthday-calendar/birthday
mvn clean install
cd ../..
cd birthday-calendar-server/serv/server
mvn clean wildfly:deploy -Dwildfly.username=boss -Dwildfly.password=PASSWORDGOESHERE

/home/pi/clean.sh
